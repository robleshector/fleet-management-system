<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Vehicle;
use App\Vehicle_model;
use App\Vehicle_status;
use Faker\Generator as Faker;

function generatePlateNumber() {
    $result = "";
    for($i = 0; $i < 6; $i++) {
        if($i < 2) {
            $result = $result . chr(rand(65,90));
        } else {
            $result = $result . rand(1,9);
        }
    }
    
    return $result;
}

$factory->define(Vehicle::class, function (Faker $faker) {
    return [
        'plate_number' => generatePlateNumber(),
        'vehicle_model_id' => rand(1, 11),
        'vehicle_status_id' => 1
    ];
});
