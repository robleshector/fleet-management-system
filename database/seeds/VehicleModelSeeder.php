<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicle_models')->insert([
            'name' => 'Vios',
            'make' => 'Toyota',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_1.png',
            'description' => 'Sedans are a good choice for most automobile shoppers. The enclosed trunk offers security, while the rear doors allow easy entry for rear-seat passengers. Most luxury vehicles are four-door sedans because they’re more comfortable than most other body styles.',
            'seats' => '5',
            'vehicle_category_id' => '2'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'City',
            'make' => 'Honda',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_2.png',
            'description' => 'Sedans are a good choice for most automobile shoppers. The enclosed trunk offers security, while the rear doors allow easy entry for rear-seat passengers. Most luxury vehicles are four-door sedans because they’re more comfortable than most other body styles.',
            'seats' => '5',
            'vehicle_category_id' => '2'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Lancer',
            'make' => 'Mitsubishi',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_3.png',
            'description' => 'Sedans are a good choice for most automobile shoppers. The enclosed trunk offers security, while the rear doors allow easy entry for rear-seat passengers. Most luxury vehicles are four-door sedans because they’re more comfortable than most other body styles.',
            'seats' => '5',
            'vehicle_category_id' => '2'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Aerox',
            'make' => 'Yamaha',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_4.png',
            'description' => 'The Yamaha Aerox 155 is a maxi-scooter with a 150 cc engine with a sporty and futuristic design. The Aerox 155 also sports some interesting features like a full-digital instrument panel, LED headlight and taillight, mobile charging socket, standard ABS and a massive 25-litres of underseat storage.' ,
            'seats' => '2',
            'vehicle_category_id' => '1'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Click',
            'make' => 'Honda',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_5.png',
            'description' => 'The Click 125i presents an aggressive and sporty design with a sleek and innovative look based on the image of the new engine featuring cutting-edge technology and an exhilarating ride, while projecting a “slim & sharp” futuristic.',
            'seats' => '2',
            'vehicle_category_id' => '1'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Hiace',
            'make' => 'Toyota',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_6.png',
            'description' => 'If you transport large amounts of cargo or need room for more than seven adults, a full-size van is your only option. They’re available with and without windows and in payload capacities of over one ton. Extended vans can seat up to 15 adult passengers. Towing packages with 8- or 10-cylinder engines will allow these rear-wheel-drive vehicles to tow large boats and trailers.',
            'seats' => '16',
            'vehicle_category_id' => '5'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Montero',
            'make' => 'Mitsubishi',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_7.png',
            'description' => 'Sport utility vehicle (SUV) is a category of motor vehicles that combine elements of road-going passenger cars with features from off-road vehicles, such as raised ground clearance and four-wheel drive.',
            'seats' => '7',
            'vehicle_category_id' => '3'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Alphard',
            'make' => 'Toyota',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_8.png',
            'description' => 'If you’re constantly carting kids or cargo, a minivan may be your best choice. Most newer models offer an additional 4th door on the driver’s side and offer comfortable seating for seven. Be aware of the different engines available.',
            'seats' => '7',
            'vehicle_category_id' => '4'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Carry',
            'make' => 'Suzuki',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_9.png',
            'description' => 'Mini truck, also called a micro-truck, are tiny but practical light trucks, available in RWD or 4WD version,',
            'seats' => '2',
            'vehicle_category_id' => '6'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Forward',
            'make' => 'Isuzu',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_10.png',
            'description' => 'A truck or lorry is a motor vehicle designed to transport cargo. Trucks vary greatly in size, power, and configuration; smaller varieties may be mechanically similar to some automobiles.',
            'seats' => '3',
            'vehicle_category_id' => '7'
        ]);
        DB::table('vehicle_models')->insert([
            'name' => 'Auman',
            'make' => 'Foton',
            'year' => rand(2012,2019),
            'image' => 'model-images/model_11.png',
            'description' => 'A truck or lorry is a motor vehicle designed to transport cargo. Trucks vary greatly in size, power, and configuration; smaller varieties may be mechanically similar to some automobiles.',
            'seats' => '3',
            'vehicle_category_id' => '8'
        ]);
    }
}
