<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicle_statuses')->insert([
            'name' => 'Available'
        ]);
        DB::table('vehicle_statuses')->insert([
            'name' => 'Not Available'
        ]);
        DB::table('vehicle_statuses')->insert([
            'name' => 'Under Maintenance'
        ]);
    }
}
