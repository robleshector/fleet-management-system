<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(VehicleCategorySeeder::class);
        $this->call(VehicleStatusSeeder::class);
        $this->call(VehicleModelSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(OrderStatusSeeder::class);
        $this->call(OrderSeeder::class);
        
        factory('App\Vehicle', 50)->create();
        factory('App\User', 25)->create();
        
        $this->call(OrderVehicleSeeder::class);
    }
}
