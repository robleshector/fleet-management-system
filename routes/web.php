<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::user() === null) {
        return redirect('/login');
    } else {
        return redirect(route('vehicle_models.index'));
    }
});


// Resources
Route::resource('vehicles', 'VehicleController');
Route::resource('departments', 'DepartmentController');
Route::resource('vehicle_models', 'VehicleModelController');
Route::resource('orders', 'OrderController');
Route::resource('order_requests', 'OrderRequestController');


Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
