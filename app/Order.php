<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function order_status() {
        return $this->belongsTo(Order_status::class);
    }

    public function vehicle_models() {
    	return $this->belongsToMany('App\Vehicle_model', 'order_vehicle_model')
    		->withPivot('quantity');
    }

        public function vehicles() {
    	return $this->belongsToMany('App\Vehicle', 'order_vehicle');
    }
}
