<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Order::class => OrderPolicy::class,
        Vehicle::class => VehiclePolicy::class,
        Vehicle_model::class => Vehicle_modelPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function($user) {
            return $user !== null && $user->role_id === 1;
        });

        Gate::define('isInspector', function($user) {
            return $user !== null && $user->role_id === 2;
        });

        Gate::define('isUser', function($user) {
            return $user !== null && $user->role_id === 3;
        });

        Gate::define('isLogged', function($user) {
            return $user !== null;
        });

        Gate::define('viewOrder', function($user, $order) {
            return $user !== null && $user->id == $order->user_id || $user !== null &&  $user->role_id !== 3;
        });

        Gate::define('viewVehicle', function($user, $vehicle) {
            
            return $vehicle->orders->where('user_id', $user->id)->first() || $user !== null &&  $user->role_id !== 3;
        });

        Gate::define('isMod', function($user) {
            return $user !== null && $user->role_id !== 3;
        });
    }
}
