<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle_model;
use Illuminate\Support\Facades\Auth;


class OrderRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user() === null && Auth::user()->role_id !== 3) {
            abort(403, 'Unauthorized action.');
        }
        
        $request->validate([
            'quantity' => 'required|min:1'
        ]);

        $quantity = (int)$request->quantity;
        
        // store to session order_request
        $request->session()->put("order_request.$id", $quantity);
        $request->session()->flash('success', 'Successfully updated your request');

        return redirect(url()->previous().'#model-'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(Auth::user() === null && Auth::user()->role_id !== 3) {
            abort(403, 'Unauthorized action.');
        }

        $request->session()->forget("order_request.$id");

        if(count($request->session()->get('order_request')) == 0) {
            $request->session()->forget('order_request');
        }

        $request->session()->flash('success', "Successfully removed vehicle from your request.");

        return redirect(route('vehicle_models.index'));
    }
}
