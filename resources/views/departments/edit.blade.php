@extends('layouts.app')

@section('content')

{{-- Start of Messages --}}
@include('messages')
{{-- End of Messages --}}

<div class="row">
    <div class="col-12 col-sm-6 mx-auto p-3">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Update {{$department->name}}</h5>
            </div>
            <form action="{{route('departments.update', ['department' => $department->id])}}" method="post">
                <div class="card-body">
                    @method('put')
                    @csrf
                    <div class="form-group col-12">
                        <label for="name">Department Name:</label>
                        <input type="text" name="name" class="form-control" placeholder="Name" value="{{$department->name}}">
                    </div>
                </div>
                <div class="card-footer text-right">
                    <a href="{{ route('departments.index') }}" class="btn text-light ml-auto">Back</a>
                    <button type="submit" class="btn btn-secondary ml-2">Update Department</button>
                </div>
            </form>
        </div>
    </div>

</div>

@endsection