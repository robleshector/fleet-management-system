@extends('layouts.app')

@section('content')

{{-- Start of Messages --}}
@include('messages')
{{-- End of Messages --}}

    <div class="col-12 col-sm-8 col-lg-6 mx-auto px-0 px-sm-3">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Departments</h5>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Department Name</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- Start of Department Items --}}
                        @foreach($departments as $department)
                        <tr>
                            <td>
                                {{$department->name}}
                            </td>
                            <td class="px-0">
                                <form action="{{route('departments.destroy', ['department' => $department->id])}}" class="form-inline flex-fill" method="post">
                                    @csrf
                                    @method('delete')
                                    <a href="{{route('departments.edit', ['department' => $department->id])}}" class="btn btn-secondary w-50 btn-sm">Update</a>
                                    <button class="btn btn-danger w-50 btn-sm">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        {{-- End of Department Items --}}
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <h5 class="text-center">Create Department</h5>
                <form action="{{route('departments.store')}}" class="form-inline" method="post">
                    @csrf
                    <div class="input-group w-100">
                        <input type="text" name="name" class="form-control" placeholder="Department Name">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary ml-auto">Create Department</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="col-12 pt-3 mx-auto">
        
    </div>

</div>

@endsection