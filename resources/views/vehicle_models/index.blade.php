@extends('layouts/app')
{{-- {{dd($vehicle_models)}} --}}

@section('title', 'Vehicles')

@section('content')

<div class="row mt-0 mt-md-4 mb-0">

    
{{-- Start of Messages --}}
@include('messages')
{{-- End of Messages --}}

</div>

<div class="row">


   {{-- START MAIN --}}
    <div class="
    @if(Auth::user() !== null && Auth::user()->can('isMod'))
        col-sm-12
    @else
        col-lg-9
    @endif
    ">
        
        <div class="row mt-3 mt-lg-0 mb-2">
            <div class="col-12 col-md-6">
                <h1>{{ Auth::user()->can('isUser')? "Choose Vehicles" : "Vehicle Models" }}</h1>
            </div>

            @can('isMod')
            <div class="col-12 col-md-6 text-md-right">
                <a href="{{route('vehicle_models.create')}}" class="btn btn-secondary">&plus; Add a Vehicle Model</a>
            </div>
            @endcan


        </div>

        <div class="row pr-lg-2">
            
            {{-- Start of Vehicle Cards --}}
            @foreach($vehicle_models as $vehicle_model)
                @include('vehicle_models.inc.model_card')
            @endforeach
            {{-- End of Vehicle Cards --}}

        </div>
    </div>
    {{-- END MAIN --}}

        {{-- START CART --}}
        @can('isUser')
        <div class="col-sm-3 accordion d-none d-lg-block request-form" id="model-accordion">
            <div class="card position-sticky sticky-top">
                <div class="card-header p-0 bg-primary" id="model-list-toggle">
                    <button class="btn btn-primary w-100" data-toggle="collapse" data-target="#model-list">Vehicle Requests</button>
                </div>
                <div class="card-body bordered collapse show py-2" id="model-list" data-parent="#model-accordion">

                    {{-- Start Vehicle Items --}}
                    @if(Session::has('order_request'))
                    @foreach($request_models as $request_model)
                        <div class="row">
                            <div class="col-12">
                                {{-- DELETE Cart Item --}}
                                <form action="{{route('order_requests.destroy', ['order_request' => $request_model->id])}}" class="float-right" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn text-danger p-0" data-toggle="tooltip" title="Remove vehicle from request">
                                        <i class="far fa-times-circle"></i>
                                    </button>
                                </form>
                            </div>
                            <div class="col-12">
                                <div class="row mb-2">
                                    <div class="col-4">
                                        <img src="/public/{{$request_model->image}}" alt="" class="img-fluid">
                                    </div>
                                    <div class="col-8">
                                        <div class="row">
                                            <p class="mb-0">{{ "$request_model->make $request_model->name" }}</p>
                                        </div>
                                        <div class="row">
                                            <small>{{$request_model->year}}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <form action="{{route('order_requests.update', ['order_request' => $request_model->id])}}" method="post">
                                    @csrf
                                    @method('put')
                                    <label for="quantity" class="mb-1">Quantity:</label>
                                    <div class="form-group flex-fill row mb-0">
                                        <div class="col-12 input-group">
                                            <input type="number" class="form-control p-1 text-center" name="quantity" value="{{$request_model->quantity}}">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary btn-sm w-100 h-100">Update</button>
                                            </div>
                                        </div>
                                        <div class="col-8 pl-0">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr>
                    @endforeach

                    
                    @else
                    <span class="text-center d-block">Your request list is empty</span>
                    @endif
                    {{-- End Vehicle Items --}}
                </div>
                
                <div class="card-header bg-primary p-0" id="formAccordion">
                    <button class="btn btn-primary w-100" type="button" data-toggle="collapse" data-target="#formCollapse" aria-expanded="true" aria-controls="formCollapse">
                    <i class="fas fa-chevron-down"></i> Next
                    </button>
                </div>
                
                {{-- REQUEST FORM --}}
                <div class="card-body bordered py-4 collapse" id="formCollapse" aria-labelledby="headingOne" data-parent="#model-accordion">
                    <form class="mb-3" action="{{ route('orders.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="borrow_date" class="mb-0">Borrow Date:</label>
                            <input type="date" name="borrow_date" class="form-control" value="{{ old('borrow_date')? old('borrow_date') : date('Y-m-d') }}">
                        </div>
                        <div class="form-group">
                            <label for="return_date" class="mb-0">Return Date:</label>
                            <input type="date" name="return_date" class="form-control" value="{{ old('return_date')? old('borrow_date') : date('Y-m-d') }}">
                        </div>
                        <div class="form-group">
                            <label for="purpose" class="mb-0">Purpose:</label>
                            <textarea name="purpose" id="purpose" rows="5" class="form-control">{{ old('purpose') }}</textarea>
                        </div>
                        <button class="btn btn-secondary w-100" type="submit">Submit Request</button>
                    </form>
                    <form action="#">
                        <button class="btn btn-danger w-100" type="submit">Cancel Request</button>
                    </form>
                </div>
                <div class="card-footer bg-primary p-3">
                </div>
            </div>
            
        </div>
        @endcan
        {{-- END CART --}}

</div>

<div class="row section px-3">
    
    
</div>
<div class="row mt-4">
    <div class="mx-auto">
        {{$vehicle_models->links()}}
    </div>
</div>

@can('isUser')
<!-- Button trigger modal -->
<button type="button" id="modal-button" class="btn btn-primary" data-toggle="modal" data-target="#request-form">
    <i class="fas fa-th-list"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="request-form" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header card-header py-1">
        <span class="modal-title" id="request-formLabel">Request Form</span>
        <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-0">
        @if(Session::has('order_request'))
        @foreach($request_models as $request_model)
            <div class="accordion" id="request-model-{{ $request_model->id }}">
                <div class="card">
                    <div class="card-header order-show py-1 px-0" id="heading-{{ $request_model->id }}">
                        <button class="btn w-100 px-1" type="button" data-toggle="collapse" data-target="#collapse-{{ $request_model->id }}">
                            {{ "$request_model->make $request_model->name" }} &times; {{ $request_model->quantity }}
                        </button>
                    </div>
                    <div id="collapse-{{ $request_model->id }}" class="collapse" data-parent="#request-model-{{ $request_model->id }}">
                        <div class="card-body p-2">
                            <div class="row">
                                <div class="col-4">
                                    <img src="/public/{{$request_model->image}}" alt="" class="img-fluid">
                                </div>
                                <div class="col-8">
                                    <form action="{{route('order_requests.update', ['order_request' => $request_model->id])}}" method="post">
                                    <label for="quantity">Quantity</label>
                                        @csrf
                                        @method('put')
                                        <div class="input-group">
                                            <input type="number" class="form-control form-control-sm p-1 text-center" name="quantity" value="{{$request_model->quantity}}">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary btn-sm w-100 h-100">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        @else
            <div class="col-12 py-4 text-center">
                Your request list is empty
            </div>
        @endif
        <div class="accordion" id="request-form">
            <div class="card">
                <div class="card-header py-1 px-0" id="form-heading">
                    <button class="btn w-100 text-light px-1" type="button" data-toggle="collapse" data-target="#form-collapse">
                        Next
                    </button>
                </div>
                <div id="form-collapse" class="collapse" data-parent="#request-form">
                    <div class="card-body p-2">
                        
                        <form class="mb-3" action="{{ route('orders.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="borrow_date" class="mb-0">Borrow Date:</label>
                                <input type="date" name="borrow_date" class="form-control" value="{{ old('borrow_date')? old('borrow_date') : date('Y-m-d') }}">
                            </div>
                            <div class="form-group">
                                <label for="return_date" class="mb-0">Return Date:</label>
                                <input type="date" name="return_date" class="form-control" value="{{ old('return_date')? old('borrow_date') : date('Y-m-d') }}">
                            </div>
                            <div class="form-group">
                                <label for="purpose" class="mb-0">Purpose:</label>
                                <textarea name="purpose" id="purpose" rows="5" class="form-control">{{ old('purpose') }}</textarea>
                            </div>
                            <button class="btn btn-secondary w-100" type="submit">Submit Request</button>
                        </form>
                        <form action="#">
                            <button class="btn btn-danger w-100" type="submit">Cancel Request</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer card-footer">
      </div>
    </div>
  </div>
</div>
@endcan

@endsection