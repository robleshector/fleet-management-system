@extends('layouts/app')

@section('title', "Update Vehicle Model")

@section('content')


<div class="row mt-4">
    
    {{-- Start of Messages --}}
    @include('messages')
    {{-- End of Messages --}}
    
    <div class="col-12 col-md-8 col-lg-6 mx-auto">
        
        <div class="row">
            <div class="col-6 text-left mb-5">
                <h2 class="mb-1">Update {{"$vehicle_model->make $vehicle_model->name $vehicle_model->year"}}</h2>
            </div>
            <div class="col-6 text-left mb-5 text-right">
                <a href="{{route('vehicle_models.index')}}" class="btn btn-secondary px-5">Back</a>
            </div>
        </div>

        <img src="/public/{{$vehicle_model->image}}" alt="" class="img-fluid">
        

        <form action="{{route('vehicle_models.update', ['vehicle_model' => $vehicle_model->id])}}" method="post" enctype="multipart/form-data">
            @method('put')
            @csrf
            
            <div class="form-group">
                <label for="name">Model Name:</label>
                <input type="text" name="name" class="form-control" value="{{$vehicle_model->name}}" required placeholder="Model Name">
            </div>

            <div class="form-group">
                <label for="make">Make:</label>
                <input type="text" name="make" class="form-control" value="{{$vehicle_model->make}}" required placeholder="Model Make">
            </div>

            <div class="row">
                <div class="form-group col-6">
                    <label for="year">Year:</label>
                    <input type="number" name="year" class="form-control" value="{{$vehicle_model->year}}" required placeholder="{{date('Y')}}">
                </div>
                
                <div class="form-group col-6">
                    <label for="year">Number of Seats:</label>
                    <input type="number" name="seats" class="form-control" value="{{$vehicle_model->seats}}" required placeholder="Number of Seats">
                </div>
            </div>

            <div class="form-group mb-3">
                <label for="category">Category:</label>
                <select name="category" class="form-control mb-2" required>
                    <option value="" selected>Choose a category</option>
                    {{-- Start of Model List --}}
                    @foreach($vehicle_categories as $category)
                    <option value="{{$category->id}}"
                        @if(old('category'))
                            {{old('category') == $category->id ? "selected" : ""}}
                            >
                            {{"$category->name"}}
                        @else
                            {{$vehicle_model->vehicle_category_id == $category->id ? "selected" : ""}}
                            >
                            {{"$category->name"}}
                        @endif

                    </option>
                    @endforeach
                    {{-- End of Model List --}}

                </select>
            </div>

            <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" name="image" id="image" class="form-control-file">
            </div>

            <div class="form-group mb-5">
                <label for="description">Description:</label>
                <textarea name="description" rows="10" class="form-control" placeholder="Description">{{$vehicle_model->description}}</textarea>
            </div>

            <button class="btn btn-primary w-100 mb-3" type="submit">Update Vehicle Model</button>

        </form>

        <a href="{{route('vehicle_models.index')}}" class="btn btn-secondary w-100">Cancel</a>


    </div>
</div>


@endsection