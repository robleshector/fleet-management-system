@extends('layouts/app')

@section('content')

{{-- MESSAGES --}}

<div class="row">

	@include('messages')

		<div class="col-sm-4 mb-2">
			
		{{-- START REQUESTED SIDE BAR --}}
		@if($order->order_status_id !== 1 || Auth::user()->role_id === 1)
	        <div class="card mb-3">
				<div class="card-header order-show">
	                <h5 class="float-left mb-0">Requested Vehicles</h5>
				</div>
				<div class="card-body overflow-scroll" id="order-request-form">

	                {{-- Start Vehicle Items --}}
	                @if($order->status_id !== 1 && $order->vehicle_models->first)
						@foreach($order->vehicle_models as $request_model)
						@php
							$approved = count($order->vehicles()->where('vehicle_model_id', $request_model->id)->get());
							$required = $request_model->pivot->quantity;
						@endphp
		                    <div class="row py-2">
		                        <div class="col-12">
		                            <div class="row mb-2">
		                                <div class="col-4">
		                                    <img src="/public/{{$request_model->image}}" alt="" class="img-fluid">
		                                </div>
		                                <div class="col-8">
	                                        <p class="mb-0">{{ "$request_model->make $request_model->name" }} | <small>{{$request_model->year}}</small></p>
											<p class="
											@if($approved < $required)
												bg-grey
											@elseif($approved = $required)
												bg-success
											@elseif($approved > $required)
												bg-danger
											@endif
											d-inline py-1 px-2 text-light">Assigned: {{ "$approved / $required" }}</p>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                @endforeach
		                @else
		                    <span class="text-center d-block">Your request list is empty</span>
	                @endif
	                {{-- End Vehicle Items --}}
	                
				</div>
				<div class="order-show card-footer">

				</div>
	        </div>
        @endif
		{{-- END REQUESTED SIDE BAR --}}


		{{-- START ORDER DETAILS --}}
			<div class="card">
				<div class="card-header order-show">
					<h5 class="mb-0 d-inline">{{ $order->order_number }}</h5>
					<div class="badge d-inline mb-1 px-2 py-1 text-light
						@if($order->order_status->id == 2 && date('Y-m-d') > $order->return_date)
							bg-danger
						@elseif($order->order_status_id === 1)
							bg-grey
						@elseif($order->order_status_id === 2)
							bg-success
						@elseif($order->order_status_id === 3)
							bg-primary
						@elseif($order->order_status_id === 4)
							bg-warning
						@endif
					">
						{{ $order->order_status->id == 2 && date('Y-m-d') > $order->return_date? "OVERDUE" : $order->order_status->name }}
					</div>
					
				</div>
				@if($order->order_status_id === 1 || Auth::user()->role_id === 1)
					<form action="{{ route('orders.update', ['order' => $order->id]) }}" method="post">
						@csrf
						@method('put')
				@endif
				<div class="card-body p-3">
					<div class="row mb-3">
						<div class="col-sm-6 border-right">
							<div class="col-12 p-0">
								<p class="mb-0">Requested by:</p>
								<p class="mb-0">{{ ucwords($order->user->firstname . " " . $order->user->lastname) }}</p>
								<p class="mb-0">Date Requested:</p>
								<p class="mb-0">{{ date('M d, Y', strtotime($order->created_at)) }}</p>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="col-12 p-0">
								<p class="mb-0">Borrow Date:</p>
								@if($order->order_status_id === 1 && Auth::user()->cannot('isMod'))
									<input type="date" name="borrow_date" class="form-control form-control-sm" value="{{ $order->borrow_date }}">
								@else
									<p class="mb-0">{{ date('F d, Y', strtotime($order->borrow_date)) }}</p>

								@endif

								<p class="mb-0">Return Date:</p>
								@if($order->order_status_id === 1 && Auth::user()->cannot('isMod'))
									<input type="date" name="return_date" class="form-control form-control-sm" value="{{ $order->return_date }}">
								@else
									<p class="mb-0">{{ date('F d, Y', strtotime($order->return_date)) }}</p>
								@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 border-top py-3">
							<p class="mb-0">Purpose: </p>

							@if($order->order_status_id === 1 && Auth::user()->id == $order->user_id)
								<textarea name="purpose" rows="5" class="form-control">{{ $order->purpose }}</textarea>
							@else
								<p>{{ $order->purpose }}</p>
							@endif

						</div>
						@can('isAdmin')
							<div class="col-12 p-3">
								<p class="mb-0">Status:</p>
								<div class="form-check form-check-inline">
									{{-- START STATUS ITEMS --}}
									@foreach($statuses as $status)
									<input type="radio" class="form-check-input" name="status" id="{{ $status->name }}" value="{{ $status->id }}" {{ $status->id == $order->order_status->id ? "checked" : "" }}>
										<label class="form-check-label mr-4" for="status-STATUS">{{ $status->name }}</label>
									@endforeach
									{{-- END STATUS ITEMS --}}
								</div>
							</div>
						@endcan
					</div>
				</div>
				@if($order->order_status_id === 1 || Auth::user()->role_id === 1)
					<div class="card-footer order-show">
						<button type="submit" class="btn btn-primary w-100">Update Order</button>
						@can('isAdmin')
						<button type="submit" class="btn btn-danger w-100 mt-3" name="reject" value="reject">Reject Order</button>
						@endcan
					</div>
				</form>
				@endif
			</div>

				@if($order->order_status->id == 1 && Auth::user()->cannot('isInspector'))
					<div class="col-12">
						<form action="{{ route('orders.destroy', ['order' => $order->id]) }}" method="post" class="text-center">
							@csrf
							@method('delete')
							<button type="submit" class="btn text-danger ml-auto font-weight-bold">Cancel Order</button>
						</form>
					</div>
				@endif
		</div>
	{{-- END ORDER DETAILS --}}
	
	    <div class="col-sm-8">
	    	
			{{-- START REQUESTED VEHICLE MAIN --}}
			@if($order->order_status_id === 1 && Auth::user()->role_id !== 1 && $order->vehicle_models->first())
	        <div class="card mb-3">
				<div class="card-header order-show">
	                <h5 class="float-left mb-0">Requested Vehicles</h5>
				</div>
				<div class="card-body">

	                {{-- Start Vehicle Items --}}
	                @if($order->vehicle_models()->first())
	                <div class="row">
	                	
	                @foreach($order->vehicle_models as $request_model)
	                    <div class="col-6 col-sm-4 p-1">
	                    	<div class="card">

								@can('isUser')
	                    		<div class="col-12 px-1">
	                    			<form action="{{ route('orders.update', ['order' => $order->id]) }}?delete={{ $request_model->id }}" class="float-right" method="post">
                    				    @csrf
                    				    @method('put')
                    				    <button class="btn text-danger p-0" data-toggle="tooltip" name="delete-model" value="delete" title="Remove from Order">
                    				        <i class="far fa-times-circle"></i>
                    				    </button>
                    				</form>
	                    		</div>
								@endcan

	                    		<img src="/public/{{ $request_model->image }}" alt="" class="img-fluid card-img-top order">
	                    		<div class="card-body p-0">
	                    			<div class="col-12 text-center">
	                    				<span class="mb-0 font-weight-bold">{{ "$request_model->make $request_model->name $request_model->year" }}</span>
	                    			</div>

									@if($order->order_status->id == 1 && Auth::user()->cannot('isInspector'))
	                    			<div class="col-12 px-0">
	                    				<form action="{{ route('orders.update', ['order' => $order->id]) }}?updateQuantity={{ $request_model->id }}" method="post">
	                    					@csrf
	                    					@method('put')
	                    					<div class="form-group flex-fill row mb-0">
			                                   <div class="col-12 input-group">
			                                       <input type="number" class="form-control p-1 text-center" name="quantity" value="{{$request_model->pivot->quantity}}" min="1">
			                                       <div class="input-group-append">
			                                           <button class="btn btn-primary btn-sm w-100 h-100">Update</button>
			                                       </div>
			                                   </div>
			                                   <div class="col-8 pl-0">
			                                   </div>
			                               </div>
	                    				</form>
	                    			</div>
									@endif

	                    		</div>
	                    	</div>
	                    </div>
	                @endforeach
	                </div>

	                @else
	                    <span class="text-center d-block">Order is empty</span>
	                @endif
	                {{-- End Vehicle Items --}}
	                
				</div>
	        </div>
	        @endif
			{{-- END REQUESTED VEHICLE MAIN --}}
			
			{{-- START ASSIGNED VEHICLES MAIN --}}
	        @if($order->order_status_id !== 1 || Auth::user()->can('isMod'))
            <div class="card mb-3">
    			<div class="card-header order-show">
                    <h5 class="float-left mb-0">Approved Vehicles</h5>
    			</div>
    			<div class="card-body overflow-scroll" id="order-approved">

                    {{-- Start Vehicle Items --}}	                
                    <div class="row">	
                    @foreach($order->vehicles as $assigned_vehicle)
                        <div class="col-6 col-sm-3 col-lg-2 p-1">
                        	<div class="card">
                        		<img src="/public/{{ $assigned_vehicle->vehicle_model->image }}" alt="" class="img-fluid card-img-top small">
                        		<div class="card-body p-0">
                        			<div class="col-12 text-center">
                        				<span class="mb-0 font-weight-bold">{{ $assigned_vehicle->plate_number }}</span>
                        			</div>
                        			<div class="col-12 px-0">
                        				<a href="{{ route('vehicles.show', ['vehicle' => $assigned_vehicle->id]) }}" class="btn btn-primary btn-sm w-100">View Details</a>
									</div>
									@can('isAdmin')
										<div class="col-12 px-0">
											<form action="{{ route('orders.update', ['order' => $order->id]) }}?unassign={{ $assigned_vehicle->id }}" method="post">
												@csrf
												@method('put')
												<button type="submit" class="btn btn-danger btn-sm w-100">Unassign</button>
											</form>
										</div>
									@endcan
                        		</div>
                        	</div>
					</div>
                    @endforeach
                    </div>
                    {{-- End Vehicle Items --}}
                    
				</div>
				<div class="card-footer order-show">
					<form action="{{route('orders.update', ['order' => $order->id])}}">
					
					</form>
				</div>
            </div>
			@endif
			{{-- END ASSIGNED VEHICLES MAIN --}}

	        {{-- START AVAILABLE VEHICLE MAIN --}}
	        @can('isAdmin')
            <div class="card">
    			<div class="card-header order-show">
                    <h5 class="float-left mb-0">Available Vehicles</h5>
    			</div>
    			<div class="card-body overflow-scroll" id="order-available">

					@if(count($vehicles) !== 0)
                    {{-- Start Vehicle Items --}}	                
                    <div class="row">	
                    @foreach($vehicles as $vehicle)
                        <div class="col-6 col-sm-4 col-lg-3 p-1">
                        	<div class="card">
                        		<img src="/public/{{ $vehicle->vehicle_model->image }}" alt="" class="img-fluid card-img-top mid">
                        		<div class="card-body p-0">
                        			<div class="col-12 text-center">
                        				<span class="mb-0 font-weight-bold">{{ $vehicle->plate_number }}</span>
                        			</div>
                        			<div class="col-12 px-0">
                        				<a href="{{ route('vehicles.show', ['vehicle' => $vehicle->id]) }}" class="btn btn-primary w-100">View Details</a>
                        			</div>
                        			<div class="col-12 px-0">
										<form action="{{ route('orders.update', ['order' => $order->id]) }}?assign={{ $vehicle->id }}&model_id={{ $vehicle->vehicle_model_id }}" method="post">
											@csrf
											@method('put')
											<button type="submit" class="btn btn-secondary w-100">Assign</button>
										</form>
                        			</div>
                        		</div>
                        	</div>
					</div>
                    @endforeach
                    </div>
                    {{-- End Vehicle Items --}}
                    @else
                    <p class="text-center mb-0">There are no more available vehicles of that type for the request dates</p>
                    @endif

    			</div>
            </div>
			
			<div class="card-footer order-show">
				<div class="mx-auto">
					{{-- {{$vehicles->links()}} --}}
				</div>
			</div>
			@endcan
			{{-- END AVAILABLE VEHICLE MAIN --}}
			
		</div>
		{{-- {{ dd($vehicles) }} --}}
		{{-- END  MAIN --}}

	
</div>

@endsection