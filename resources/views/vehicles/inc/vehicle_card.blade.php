<?php $vehicle_model = $vehicle->vehicle_model ?>

@can('viewVehicle', $vehicle)
<div class="col-sm-6 col-md-4 col-lg-3 p-3 p-md-1">
    <div class="card">
        <div class="col-12 px-0">
            <img src="/public/{{$vehicle_model->image}}" alt="" class="img-fluid card-img-top vehicle-img">
        </div>
        <div class="card-body p-0">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 text-center">
                        <h5 class="mb-0">{{$vehicle->plate_number}}</h5>
                    </div>
                    <div class="col-12
                        @if($vehicle->vehicle_status_id == 1)
                        bg-success 
                        @elseif($vehicle->vehicle_status_id == 2)
                        bg-danger 
                        @elseif($vehicle->vehicle_status_id == 3)
                        bg-grey 
                        @endif
                        mb-3 text-center text-light font-weight-bold">{{$vehicle->vehicle_status->name}}
                    </div>
                    <div class="col-12">
                        <h5 class="mb-0">{{$vehicle_model->make}} {{$vehicle_model->name}} | <small>{{$vehicle_model->year}}</small></h5>
                        <p class="mb-0">Category: {{$vehicle_model->vehicle_category->name}} </p>
                        <p>Seats: {{$vehicle_model->seats}}</p>
                    </div>
                    <div class="col-12 px-0">
                        <a href="{{route('vehicles.show', ['vehicle' => $vehicle->id])}}" class="btn btn-primary text-light d-inline-block w-100 py-2">Vehicle Details</a>
                    </div>
        
                    @can('isMod')
                        <div class="col-6 px-0">
                            <a href="{{route('vehicles.edit', ['vehicle' => $vehicle->id])}}" class="btn btn-secondary text-light d-inline-block w-100 py-2">Update Vehicle</a>
                        </div>
                        <div class="col-6 px-0">
                            <form action="{{route('vehicles.destroy', ['vehicle' => $vehicle->id])}}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger w-100 py-2">Delete Vehicle</button>
                            </form>
                        </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endcan