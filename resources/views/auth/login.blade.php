@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8 mt-3">
            <div class="card">
                <div class="card-body">
                    <h5>Welcome!</h5>
                    <p>This fleet management system web app is a sample project that uses PHP and Laravel framework. The idea is for a user to be able to request a vehicle for a specific date range, for an admin to be able to assign vehicles depending on the vehicle availability for the requested dates and for an inspector to be able to set the status of a vehicle upon return.</p>
                    <div class="row border-top mt-3 pt-3">
                        <div class="col-12 text-center">
                            <h5 class="mb-3">Sample Login Credentials</h5>
                        </div>
                        <div class="col-md-4 mb-3 mb-md-0 text-center">
                            <p class="font-weight-bold mb-0">User</p>
                            <p class="mb-0">u: user@email.com</p>
                            <p class="mb-0">p: password123</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <p class="font-weight-bold mb-0">Administrator</p>
                            <p class="mb-0">u: admin@email.com</p>
                            <p class="mb-0">p: password123</p>
                        </div>
                        <div class="col-md-4 mb-3 mb-md-0 text-center">
                            <p class="font-weight-bold mb-0">Inspector</p>
                            <p class="mb-0">u: inspector@email.com</p>
                            <p class="mb-0">p: password123</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
